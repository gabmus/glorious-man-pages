ifeq ($(PREFIX),)
    PREFIX := /usr
endif

%.1: %.1.md
	pandoc --standalone --to man $*.1.md -o $*.1

all: gmmk.1
all: model-o.1

install: gmmk.1
install: model-o.1
	install -d $(DESTDIR)$(PREFIX)/share/man/man1
	install -m 644 gmmk.1 $(DESTDIR)$(PREFIX)/share/man/man1
	install -m 644 model-o.1 $(DESTDIR)$(PREFIX)/share/man/man1

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/share/man/man1/gmmk.1
	rm -f $(DESTDIR)$(PREFIX)/share/man/man1/model-o.1

clean:
	rm -f *.1
