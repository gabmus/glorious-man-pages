% GMMK(1) Version 1.0 | Manual for the Glorious Modular Mechanical Keyboard

DESCRIPTION
===========

Documentation for the keyboard shortcuts for the GMMK keyboard lineup.

Media controls
==============

`F1`

:   open file manager

`F2`

:   open browser

`F3`

:   open calculator

`F4`

:   open music player

`F5`

:   music, previous track

`F6`

:   music, next track

`F7`

:   music, play/pause

`F8`

:   music, stop

`F9`

:   mute

`F10`

:   volume down

`F11`

:   volume up

Lighting
========

Controls
--------

`Fn + [←/→]`

:   Speed down/up

`Fn + [↑/↓]`

:   Brightness up/down

`Fn + [+]`

:   Change color

`Fn + [-]`

:   Change effect direction

Effects
-------

`Fn + [Ins]`

:   Spectrum / Breathing / Static (no FX)

`Fn + [Del]`

:   Wave / Soft wave / Spinning wave

`Fn + [Home]`

:   Ripple / Reactive / Row ripple (laser)

`Fn + [Page Down]`

:   Diagonal Wave / Static (FX) / Central radiating wave

`Fn + [End]`

:   Starlight (with black keys) / Starlight (no black keys) / Vertical wave

`Fn + [Page Up]`

:   Sinusoid / Heartbeat / Matrix

``Fn + [`]``

:   Custom / Edit custom

Customize
---------

``Fn + [`]``

:   Enter/exit custom editing mode

`Fn + [+]`

:   Change color

AUTHOR
======

Gabriele Musco <gabmus@disroot.org>

SEE ALSO
========

**glorious-model-o(1)**
