% MODEL-O(1) Version 1.0 | Manual for the Glorious Model O mouse

DESCRIPTION
===========

Documentation for the on the fly configuration for the Glorious Model O mouse.

Lighting
========

Controls
--------

Change effect

:   Middle click + Forward + DPI

Change brightness

:   Middle click + DPI + Back 

Change color

:   Middle click + DPI + Left click 

Effects
-------

1. Glorious mode (Rainbow wave)
2. Seamless breathing RGB (Spectrum)
3. Breathing RGB
4. Single color
5. Breathing single color
6. Tail (RGB chasing effect)
7. Rave (blue and red flashing, police car)
8. Wave (random/pastel RGB cycling)
9. Off
